﻿using Converters.Entities;
using Converters.Helper;
using ConvertFormat.Converters.Abstract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;

namespace Converters
{
	class Program
	{
		static void Main(string[] args)
		{
			// Example relative Source Files\DocumentExample.xml

			bool showMenu = true;
			while (showMenu)
			{
				try
				{
					showMenu = StartProcessConvertingFile();
				}
				catch (Exception ex)
				{
					Console.WriteLine($"Error: {ex.Message}");
					Console.WriteLine();
				}
			}
		}

		private static bool StartProcessConvertingFile()
		{
			DataSourceType dataSourceType = ChooseDataSourceType();

			Console.Write($"Input source file path: ");
			var sourceFileName = Console.ReadLine();

			string fileContent = ReadFileByDataSourceType(dataSourceType, sourceFileName);

			var availableConverters = ConverterHelper.GetAllConverter();

			FormatConverter converterSrc = ChooseConverterByFile(availableConverters, sourceFileName);

			FormatConverter converterDest = ChooseConverterByName(availableConverters);

			var document = converterSrc.ConvertFromString<Document>(fileContent);
			var output = converterDest.ConvertToString(document);

			Console.Write($"Input target file path: ");
			var targetFileName = Console.ReadLine().ToLower();

			WriteToFile(targetFileName, output);

			Console.WriteLine();

			return true;
		}

		private static string ReadFileByDataSourceType(DataSourceType dataSourceType, string sourceFileName)
		{
			var fileContent = string.Empty;

			switch (dataSourceType)
			{
				case DataSourceType.file:
					fileContent = ReadFile(sourceFileName);
					break;
				case DataSourceType.http:
					fileContent = ReadHttp(sourceFileName);
					break;
				case DataSourceType.cloud:
					fileContent = ReadCloud(sourceFileName);
					break;
				default:
					throw new Exception("Cant find suitable data source");
			}

			return fileContent;
		}

		private static DataSourceType ChooseDataSourceType()
		{
			var variants = Enum.GetValues(typeof(DataSourceType)).Cast<DataSourceType>().Select(x => x.ToString());

			Console.WriteLine($"Choose data source: {string.Join(", ", variants)}");

			var dataSource = Console.ReadLine().ToLower();
			if (Enum.IsDefined(typeof(DataSourceType), dataSource))
				return (DataSourceType)Enum.Parse(typeof(DataSourceType), dataSource);
			else
				throw new Exception($"Dont find data source type '{dataSource}'");
		}

		private static void WriteToFile(string targetFileName, string output)
		{
			try
			{
				if (!Path.IsPathRooted(targetFileName))
					targetFileName = Path.Combine(Directory.GetCurrentDirectory(), targetFileName);

				if (!File.Exists(targetFileName))
				{
					File.WriteAllText(targetFileName, output);
					Console.WriteLine($"File successfully created to path '{targetFileName}'");
				}
				else
				{
					throw new Exception($"File exist '{targetFileName}'");
				}
			}
			catch (Exception ex)
			{
				throw new Exception($"Cant write content to file. Error: {ex.Message}");
			}
		}

		private static string ReadFile(string sourceFileName)
		{
			try
			{
				if (!Path.IsPathRooted(sourceFileName))
					sourceFileName = Path.Combine(Directory.GetCurrentDirectory(), sourceFileName);

				if (File.Exists(sourceFileName))
				{
					using (FileStream sourceStream = File.Open(sourceFileName, FileMode.Open))
					{
						var reader = new StreamReader(sourceStream);
						var input = reader.ReadToEnd();

						return input;
					}
				}
				else
					throw new Exception($"File not exist '{sourceFileName}'");
			}
			catch (Exception ex)
			{
				throw new Exception($"Cant read content from file. Error: {ex.Message}");
			}
		}

		private static string ReadHttp(string sourceFileName)
		{
			using (HttpClient client = new HttpClient())
			{
				var sourceTextHttp = client.GetStringAsync(sourceFileName);
				return sourceTextHttp.Result;
			}
		}

		//HACK: Impl
		private static string ReadCloud(string sourceFileName)
		{
			using (WebClient client = new WebClient())
			{
				var sourceText = client.DownloadString(sourceFileName);
				return sourceText;
			}
		}

		private static FormatConverter ChooseConverterByFile(IEnumerable<FormatConverter> availableConverters, string filePath)
		{
			var extension = Path.GetExtension(filePath);
			FormatConverter converter = availableConverters.FirstOrDefault(x => x.SupportedExtensions.Contains(extension));
			if (converter == null)
			{
				Console.WriteLine("Cant extract file extnsion. But you can try choose converter by name.");

				converter = ChooseConverterByName(availableConverters);

				if (converter == null)
					throw new Exception($"Конвертер для файла '{filePath}' не найден");
			}

			return converter;
		}

		private static FormatConverter ChooseConverterByName(IEnumerable<FormatConverter> availableConverters)
		{
			ConsoleWriteAvailableConverters(availableConverters);

			FormatConverter converter = null;

			while (converter == null)
			{
				Console.Write($"Input targer Converter name or 'exit': ");
				var targetConverterName = Console.ReadLine().ToLower();

				if (targetConverterName == "exit")
					Environment.Exit(0);

				converter = availableConverters.FirstOrDefault(x => x.Name.ToLower() == targetConverterName);
				if (converter == null)
					Console.WriteLine($"Converter with name {targetConverterName} not found ");
			}

			return converter;
		}

		private static void ConsoleWriteAvailableConverters(IEnumerable<FormatConverter> availableConverters)
		{
			Console.WriteLine("Available converters:");

			foreach (var convert in availableConverters)
			{
				Console.WriteLine($"\t{convert.Name}");
			}

			Console.WriteLine();
		}
	}
}