﻿using ConvertFormat.Converters.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Converters.Helper
{
	public static class ConverterHelper
	{
		public static IEnumerable<FormatConverter> GetAllConverter()
		{
			var types = GetAllConverterTypes();

			var converters = types.Select(type => (FormatConverter)Activator.CreateInstance(type));

			return converters;
		}

		private static IEnumerable<Type> GetAllConverterTypes()
		{
			var types = Assembly.GetAssembly(typeof(FormatConverter))
				.GetTypes()
				.Where(myType => myType.IsClass
					&& !myType.IsAbstract
					&& myType.IsSubclassOf(typeof(FormatConverter)));

			return types;
		}
	}
}
