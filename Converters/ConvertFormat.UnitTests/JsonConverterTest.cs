using ConvertFormat.Converters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ConvertFormat.UnitTests
{
	[TestClass]
	public class JsonConverterTest
	{
		[TestMethod]
		public void ConvertFromString_AllProperties()
		{
			//Arrange
			var converter = new JsonConverter();
			var expectedTitle = "exampleTitle";
			var expectedText = "exampleText";
			string input = $"{{\"Title\": \"{expectedTitle}\",\"Text\": \"{expectedText}\"}}";

			//Act
			var document = converter.ConvertFromString<Document>(input);

			//Assert
			Assert.IsNotNull(document);
			Assert.AreEqual(expectedTitle, document.Title);
			Assert.AreEqual(expectedText, document.Text);
		}

		[TestMethod]
		public void ConvertFromString_Part_Properties()
		{
			//Arrange
			var converter = new JsonConverter();
			var expectedText = "exampleText";
			string input = $"{{\"Text\": \"{expectedText}\"}}";

			//Act
			var document = converter.ConvertFromString<Document>(input);

			//Assert
			Assert.IsNotNull(document);
			Assert.AreEqual(expectedText, document.Text);
		}

		[TestMethod]
		public void ConvertFromString_Empty_Input()
		{
			//Arrange
			var converter = new JsonConverter();
			string input = string.Empty;

			//Act
			var document = converter.ConvertFromString<Document>(input);

			//Assert
			Assert.IsNull(document);
		}

		[TestMethod]
		public void ConvertFromString_Null_Input()
		{
			//Arrange
			var converter = new JsonConverter();
			string input = null;

			//Act
			//Assert
			Assert.ThrowsException<ArgumentNullException>(() => converter.ConvertFromString<Document>(input));
		}

		[TestMethod]
		public void ConvertFromString_Input_InvalidFormat()
		{
			//Arrange
			var converter = new JsonConverter();
			string input = $"{{\"Text\": }}";

			//Act
			//Assert
			Assert.ThrowsException<Exception>(() => converter.ConvertFromString<Document>(input), "Cant read json");
		}

		[TestMethod]
		public void ConvertToString_AllProperties()
		{
			//Arrange
			var converter = new JsonConverter();
			var expectedTitle = "exampleTitle";
			var expectedText = "exampleText";
			string expectedOutput = $"{{\"Title\":\"{expectedTitle}\",\"Text\":\"{expectedText}\"}}";
			var doc = new Document() { Title = expectedTitle, Text = expectedText };

			//Act
			var output = converter.ConvertToString<Document>(doc);

			//Assert
			Assert.IsNotNull(output);
			Assert.AreEqual(expectedOutput, output);
		}

		[TestMethod]
		public void ConvertToString_Part_Properties()
		{
			//Arrange
			var converter = new JsonConverter();
			var expectedTitle = "exampleTitle";
			string expectedOutput = $"{{\"Title\":\"{expectedTitle}\",\"Text\":null}}";
			var doc = new Document() { Title = expectedTitle };

			//Act
			var output = converter.ConvertToString<Document>(doc);

			//Assert
			Assert.IsNotNull(output);
			Assert.AreEqual(expectedOutput, output);
		}

		[TestMethod]
		public void ConvertToString_Null_Input()
		{
			//Arrange
			var converter = new JsonConverter();
			Document doc = null;

			//Act
			var output = converter.ConvertToString<Document>(doc);

			//Assert
			Assert.IsNull(output);
		}
	}
}
