using ConvertFormat.Converters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ConvertFormat.UnitTests
{
	[TestClass]
	public class XmlConverterTest
	{
		[TestMethod]
		public void ConvertFromString_AllProperties()
		{
			//Arrange
			var converter = new XmlConverter();
			var expectedTitle = "exampleTitle";
			var expectedText = "exampleText";
			string input = $"<Document><Title>{expectedTitle}</Title><Text>{expectedText}</Text></Document>";

			//Act
			var document = converter.ConvertFromString<Document>(input);

			//Assert
			Assert.IsNotNull(document);
			Assert.AreEqual(expectedTitle, document.Title);
			Assert.AreEqual(expectedText, document.Text);
		}

		[TestMethod]
		public void ConvertFromString_Part_Properties()
		{
			//Arrange
			var converter = new XmlConverter();
			var expectedText = "exampleText";
			string input = $"<Document><Text>{expectedText}</Text></Document>";

			//Act
			var document = converter.ConvertFromString<Document>(input);

			//Assert
			Assert.IsNotNull(document);
			Assert.AreEqual(expectedText, document.Text);
		}

		[TestMethod]
		public void ConvertFromString_Empty_Input()
		{
			//Arrange
			var converter = new XmlConverter();
			string input = string.Empty;

			//Act
			//Assert
			Assert.ThrowsException<InvalidOperationException>(() => converter.ConvertFromString<Document>(input));
		}

		[TestMethod]
		public void ConvertFromString_Null_Input()
		{
			//Arrange
			var converter = new XmlConverter();
			string input = null;

			//Act
			//Assert
			Assert.ThrowsException<ArgumentNullException>(() => converter.ConvertFromString<Document>(input));
		}

		[TestMethod]
		public void ConvertFromString_Input_InvalidFormat()
		{
			//Arrange
			var converter = new XmlConverter();
			string input = $"<Document><Text></Document>";
			//Act
			//Assert
			Assert.ThrowsException<InvalidOperationException>(() => converter.ConvertFromString<Document>(input));
		}

		[TestMethod]
		public void ConvertToString_AllProperties()
		{
			//Arrange
			var converter = new XmlConverter();
			var expectedTitle = "exampleTitle";
			var expectedText = "exampleText";
			string expectedOutput = $"<Title>exampleTitle</Title><Text>exampleText</Text></Document>";
			var doc = new Document() { Title = expectedTitle, Text = expectedText };

			//Act
			var output = converter.ConvertToString<Document>(doc);
			var outputLines = output.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
			var outputWithoutNewLine = string.Join("", outputLines);

			//Assert
			Assert.IsNotNull(output);
			Assert.AreEqual(outputLines.Length, 5);
			Assert.AreEqual("<?xml version=\"1.0\" encoding=\"utf-16\"?>", outputLines[0].Trim());
			Assert.AreEqual("<Document xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">", outputLines[1].Trim());
			Assert.AreEqual("<Title>exampleTitle</Title>", outputLines[2].Trim());
			Assert.AreEqual("<Text>exampleText</Text>", outputLines[3].Trim());
			Assert.AreEqual("</Document>", outputLines[4].Trim());
		}

		[TestMethod]
		public void ConvertToString_Null_Input()
		{
			//Arrange
			var converter = new XmlConverter();
			Document doc = null;
			var expectedOutput = "<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n<Document xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xsi:nil=\"true\" />";
			//Act
			var output = converter.ConvertToString<Document>(doc);

			//Assert
			Assert.AreEqual(expectedOutput, output);
		}
	}
}
