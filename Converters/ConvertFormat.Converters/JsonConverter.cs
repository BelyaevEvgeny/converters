﻿using Newtonsoft.Json;
using ConvertFormat.Converters.Abstract;
using System;

namespace ConvertFormat.Converters
{
	public class JsonConverter : FormatConverter
	{
		public override string Name => "json";
		public override string[] SupportedExtensions => new[] { ".json" };

		public override T ConvertFromString<T>(string input)
		{
			try
			{
				var result = JsonConvert.DeserializeObject<T>(input);

				return result;
			}
			catch (JsonReaderException ex)
			{
				throw new Exception("Cant read json", ex);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public override string ConvertToString<T>(T obj)
		{
			if (obj == null)
				return null;

			var result = JsonConvert.SerializeObject(obj);

			return result;
		}
	}
}
