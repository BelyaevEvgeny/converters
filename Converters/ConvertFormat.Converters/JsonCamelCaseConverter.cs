﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace ConvertFormat.Converters
{
	public class JsonCamelCaseConverter : JsonConverter
	{
		public override string Name => "jsonCamelCase";
		public override string ConvertToString<T>(T obj)
		{
			var contractResolver = new DefaultContractResolver { NamingStrategy = new CamelCaseNamingStrategy() };
			var jsonSerializerSettings = new JsonSerializerSettings
			{
				ContractResolver = contractResolver,
				Formatting = Formatting.Indented
			};

			var result = JsonConvert.SerializeObject(obj, jsonSerializerSettings);

			return result;
		}
	}
}
