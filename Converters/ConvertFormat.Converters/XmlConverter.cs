﻿using ConvertFormat.Converters.Abstract;
using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace ConvertFormat.Converters
{
	public class XmlConverter : FormatConverter
	{
		private readonly Encoding _encoding = Encoding.Unicode;

		public override string Name => "xml";

		public override string[] SupportedExtensions => new[] { ".xml" };

		public override T ConvertFromString<T>(string input)
		{
			var inputBytes = _encoding.GetBytes(input);
			using (MemoryStream memStream = new MemoryStream(inputBytes))
			{
				XmlSerializer serializer = new XmlSerializer(typeof(T));
				var result = (T)serializer.Deserialize(memStream);
				return result;
			}
		}

		public override string ConvertToString<T>(T obj)
		{
			using (var stringwriter = new StringWriter())
			{
				var serializer = new XmlSerializer(typeof(T));
				serializer.Serialize(stringwriter, obj);
				return stringwriter.ToString();
			}
		}
	}
}
