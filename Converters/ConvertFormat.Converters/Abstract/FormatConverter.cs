﻿namespace ConvertFormat.Converters.Abstract
{
	public abstract class FormatConverter
	{
		public abstract string Name { get; }
		public abstract string[] SupportedExtensions { get; }

		public abstract T ConvertFromString<T>(string input);

		public abstract string ConvertToString<T>(T obj) where T : class;
	}
}
